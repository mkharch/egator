-module(egator_edge).
-behaviour(gen_server).
-define(SERVER, ?MODULE).

-define(EGATOR_EDGE_PORT, 5101).

-define(TICKET_SIZE, 8).

-record(st, {sock,subs,pends}).

%% ------------------------------------------------------------------
%% API Function Exports
%% ------------------------------------------------------------------

-export([start_link/0]).

-export([new_ticket/0]).
-export([wait_for_edge/1,wait_for_edge/2]).

-export([extra_spec/1]).
-export([signal_ready/0]).

%% ------------------------------------------------------------------
%% gen_server Function Exports
%% ------------------------------------------------------------------

-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3]).

%% ------------------------------------------------------------------
%% API Function Definitions
%% ------------------------------------------------------------------

start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

new_ticket() ->
	gen_server:call(?SERVER, new_ticket).

wait_for_edge(Ticket) ->
	wait_for_edge(Ticket, infinity).

wait_for_edge(Ticket, Timeout) ->
	gen_server:call(?SERVER, {wait_for_edge,Ticket}, Timeout).

extra_spec(Ticket) ->
	MyIpAddr = my_ip_address(),
	TicketHex = [io_lib:format("~2.16.0b", [X]) || <<X>> <= Ticket],
	io_lib:format("-ready ~s:~w:~s",
		[inet_parse:ntoa(MyIpAddr),?EGATOR_EDGE_PORT,TicketHex]).

signal_ready() ->
	case init:get_argument(ready) of
	error ->
		ok;

	{ok,[[EdgeSpec]]} ->
		[Host,PortStr,TicketHex] = string:tokens(EdgeSpec, ":"),
		Port = list_to_integer(PortStr),
		Ticket = <<<<(list_to_integer([I1,I2], 16))>>
					|| <<I1,I2>> <= list_to_binary(TicketHex)>>,
		{ok,Tmp} = gen_udp:open(0),
		ok = gen_udp:send(Tmp, Host, Port, Ticket),
		ok = gen_udp:close(Tmp)
	end.

%% ------------------------------------------------------------------
%% gen_server Function Definitions
%% ------------------------------------------------------------------

init(_Args) ->
	{ok,Sock} = gen_udp:open(?EGATOR_EDGE_PORT),
    {ok,#st{sock =Sock,
			subs =[],
			pends =[]}}.

handle_call(new_ticket, _From, #st{subs =Subs,pends =Pends} =St) ->
	Ticket = crypto:rand_bytes(?TICKET_SIZE),
	false = lists:keyfind(Ticket, 1, Subs),
	false = lists:member(Ticket, Pends),
    {reply,Ticket,St};

handle_call({wait_for_edge,Ticket}, From, #st{subs =Subs,
											  pends =Pends} =St) ->
	case lists:member(Ticket, Pends) of
	true ->
		%% signal already delivered
		{reply,ready,St#st{pends =list:delete(Ticket, Pends)}};
	false ->
		{noreply,St#st{subs =[{Ticket,From}|Subs]}}
	end.

handle_cast(_Msg, St) ->
    {noreply, St}.

handle_info({udp,_Sock,_Host,_Port,Ticket}, #st{subs =Subs,
												pends =Pends} =St) ->
	%%io:format("egator_edge: signal ~p~n", [Ticket]),
	case lists:keytake(Ticket, 1, Subs) of
	false ->
		%% nobody waits for this signal (yet)
		{noreply,St#st{pends =[Ticket|Pends]}};
	{value,{_,From},Subs1} ->
		gen_server:reply(From, ready),
		{noreply,St#st{subs =Subs1}}
	end.

terminate(_Reason, _St) ->
    ok.

code_change(_OldVsn, St, _Extra) ->
    {ok, St}.

%% ------------------------------------------------------------------
%% Internal Function Definitions
%% ------------------------------------------------------------------

my_ip_address() ->
	{ok,Ifs} = inet:getifaddrs(),
	[Flags|_] = [Flags || {Ifname,Flags} <- Ifs, Ifname =/= "lo"],
	[IpAddr|_] = [IpAddr || {addr,IpAddr} <- Flags],
	IpAddr.

%%EOF
